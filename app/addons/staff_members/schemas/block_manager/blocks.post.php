<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/
if (fn_allowed_for('ULTIMATE')) {
    $schema['staff_members'] = array (
        'templates' => array(
            'addons/staff_members/blocks/staff_members.tpl' => array(
                'settings' => array(
                    'not_scroll_automatically' => array (
                        'type' => 'checkbox',
                        'default_value' => 'N'
                    ),
                    'scroll_per_page' =>  array (
                        'type' => 'checkbox',
                        'default_value' => 'N'
                    ),
                    'speed' =>  array (
                        'type' => 'input',
                        'default_value' => 400
                    ),
                    'pause_delay' =>  array (
                        'type' => 'input',
                        'default_value' => 3
                    ),
                    'item_quantity' =>  array (
                        'type' => 'input',
                        'default_value' => 4
                    ),
                    'thumbnail_width' =>  array (
                        'type' => 'input',
                        'default_value' => 200
                    ),
                    'outside_navigation' => array (
                        'type' => 'checkbox',
                        'default_value' => 'Y'
                    ),
                ),
            )
        ),
        'wrappers' => 'blocks/wrappers',
        'content' => array(
            'members' => array(
                'type' => 'function',
                'function' => array('fn_get_staff_members_all'),
            )
        ), 
    );
}
return $schema;
