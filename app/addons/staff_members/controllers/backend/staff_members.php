<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']	== 'POST') {

    fn_trusted_vars('staff_members', 'staff_data');
    $suffix = '';

    //
    // Delete members
    //
    if ($mode == 'm_delete') {
        foreach ($_REQUEST['member_ids'] as $v) {
            fn_delete_staff_member_by_id($v);
        }

        $suffix = '.manage';
    }

    //
    // Add/edit members
    //
    if ($mode == 'update') {

        $staff_id = fn_staff_members_update($_REQUEST['staff_data'], $_REQUEST['staff_id'], DESCR_SL);

        $suffix = ".update?staff_id=$staff_id";
    }

    if ($mode == 'delete') {
        if (!empty($_REQUEST['staff_id'])) {
            fn_delete_staff_member_by_id($_REQUEST['staff_id']);
        }

        $suffix = '.manage';
    }
    return array(CONTROLLER_STATUS_OK, 'staff_members' . $suffix);
}

if ($mode == 'update') {

    $staff_member = fn_get_staff_members_data($_REQUEST['staff_id'], DESCR_SL);

    if (empty($staff_member)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    Registry::set('navigation.tabs', array (
        'general' => array (
            'title' => __('general'),
            'js' => true
        ),
    ));

    Tygh::$app['view']->assign('staff_members', $staff_member);

} elseif ($mode == 'manage') {

    list($staff_members, $search) = fn_get_staff_members($_REQUEST, DESCR_SL);

    Tygh::$app['view']->assign('staff_members', $staff_members);
    Tygh::$app['view']->assign('search', $search);
    
}

