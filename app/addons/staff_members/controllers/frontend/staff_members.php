<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'update_email') {
        $staff_id = $_REQUEST['staff_id'];
        $staff_email = db_get_field("SELECT email FROM ?:staff WHERE staff_id = ?s", $staff_id);
        if (empty($staff_email)) {
            $user_id = db_get_field("SELECT user_id FROM ?:staff WHERE staff_id = ?s", $staff_id);
            if ($user_id != 0) {
                 $staff_email = db_get_field("SELECT email FROM ?:users WHERE user_id = ?i", $user_id);
            }  
        }
        echo($staff_email);
        exit;
    }
}