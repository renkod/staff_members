<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\BlockManager\Block;
use Tygh\Languages\Languages;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_get_staff_members($params = array(), $lang_code = CART_LANGUAGE)
{

    $fields = array (
        '?:staff.staff_id',
        '?:staff.status',
        '?:staff.email',
        '?:staff.user_id',
        '?:staff_descriptions.firstname',
        '?:staff_descriptions.lastname',
        '?:staff_descriptions.function',
    );

    $sortings = array(
        'name' => "?:staff_descriptions.firstname",
        'function' => "?:staff_descriptions.function",
        'status' => "?:staff.status",
        'email' => "?:staff.email",
    );

    $sorting = db_sort($params, $sortings, 'name', 'asc');

    $staff_members = db_get_hash_array("SELECT ?p FROM ?:staff LEFT JOIN ?:staff_descriptions ON ?:staff_descriptions.staff_id = ?:staff.staff_id AND ?:staff_descriptions.lang_code = ?s" . " $sorting", 'staff_id', implode(", ", $fields), $lang_code); 
    
    foreach ($staff_members as $key => &$value) {
        if ($value['user_id'] != 0) {
            $value['firsttname'] = (empty($value['firstname'])) ? db_get_field("SELECT firstname FROM ?:users WHERE user_id = ?i", $value['user_id']) : $value['firstname'];
            $value['lastname'] = (empty($value['lastname'])) ? db_get_field("SELECT lastname FROM ?:users WHERE user_id = ?i", $value['user_id']) : $value['lastname'];
            $value['email'] = (empty($value['email'])) ? db_get_field("SELECT email FROM ?:users WHERE user_id = ?i", $value['user_id']) : $value['email'];
        }     
    }

    return array($staff_members, $params);
}


function fn_delete_staff_member_by_id($staff_id)
{
    if (!empty($staff_id)) {
        fn_delete_image_pairs($staff_id, 'staff');
        db_query("DELETE FROM ?:staff WHERE staff_id = ?i", $staff_id);
        db_query("DELETE FROM ?:staff_descriptions WHERE staff_id = ?i", $staff_id);
        Block::instance()->removeDynamicObjectData('staff_members', $staff_id);   
    }
}


function fn_staff_members_update($data, $staff_id, $lang_code = DESCR_SL)
{
    if (!empty($staff_id)) {
        $data['function'] = strip_tags($data['function']);
        $data['user_id'] = db_get_field("SELECT user_id FROM ?:users WHERE user_id = ?i", $data['user_id']);
        db_query("UPDATE ?:staff SET ?u WHERE staff_id = ?i", $data, $staff_id);
        db_query("UPDATE ?:staff_descriptions SET ?u WHERE staff_id = ?i AND lang_code = ?s", $data, $staff_id, $lang_code);

        if (fn_staff_members_need_image_update()) {
            fn_delete_image_pairs($staff_id, 'staff');
        }

        $pair_data = fn_attach_image_pairs('staff_member', 'staff', $staff_id);
        fn_staff_members_image_all_links($staff_id, $pair_data);
        

    } else {
        $data['function'] = strip_tags($data['function']);
        $staff_id = $data['staff_id'] = db_query("REPLACE INTO ?:staff ?e", $data);       
        foreach (Languages::getAll() as $data['lang_code'] => $v) {
            db_query("REPLACE INTO ?:staff_descriptions ?e", $data);
        }

        if (fn_staff_members_need_image_update()) {
            $pair_data = fn_attach_image_pairs('staff_member', 'staff', $staff_id);
            if (!empty($pair_data)) {
                fn_staff_members_image_all_links($staff_id, $pair_data);
            }
        }
    }      
    return $staff_id;
}


function fn_get_staff_members_data($staff_id, $lang_code = DESCR_SL)
{
    $fields = $joins = array();
    $condition = '';

    $fields = array (
        '?:staff.staff_id',
        '?:staff.status',
        '?:staff.email',
        '?:staff.user_id',
        '?:staff_descriptions.firstname',
        '?:staff_descriptions.lastname',
        '?:staff_descriptions.function',
    );

    $joins[] = db_quote("LEFT JOIN ?:staff_descriptions ON ?:staff_descriptions.staff_id = ?:staff.staff_id AND ?:staff_descriptions.lang_code = ?s", $lang_code);

    $condition = db_quote("WHERE ?:staff.staff_id = ?i", $staff_id);
    $condition .= (AREA == 'A') ? '' : " AND ?:staff.status IN ('A', 'H')";

    $staff_member = db_get_row("SELECT " . implode(", ", $fields) . " FROM ?:staff " . implode(" ", $joins) ." $condition");

    if (!empty($staff_member)) {
        $staff_member['main_pair'] = fn_get_image_pairs($staff_member['staff_id'], 'staff', 'M', true, false);
    }
    return $staff_member;
}

function fn_staff_members_image_all_links($staff_id, $pair_data)
{
    if (!empty($pair_data)) {
        $pair_id = reset($pair_data);
        fn_add_image_link($staff_id, $pair_id);
    }
}

function fn_staff_members_need_image_update()
{
    if (!empty($_REQUEST['file_staff_member_image_icon']) && array($_REQUEST['file_staff_member_image_icon'])) {
        $image_staff = reset ($_REQUEST['file_staff_member_image_icon']);

        if ($image_staff == 'staff_member') {
            return false;
        }
    }
    return true;
}

function fn_staff_members_delete_image_pre($image_id, $pair_id, $object_type)
{
    if ($object_type == 'staff') {
        $staff_image_ids = db_get_fields("SELECT object_id FROM ?:images_links WHERE image_id = ?i AND object_type = 'staff'", $image_id);
        if (!empty($staff_image_ids)) {
            db_query("DELETE FROM ?:images_links WHERE object_id IN (?a)", $staff_image_ids);
        }
    }
}

function fn_get_staff_members_all()
{
    $lang_code = DESCR_SL;
    $fields = $joins = array();
    $condition = '';

    $fields = array (
        '?:staff.staff_id',
        '?:staff.status',
        '?:staff.email',
        '?:staff.user_id',
        '?:staff_descriptions.firstname',
        '?:staff_descriptions.lastname',
        '?:staff_descriptions.function',
    );

    $joins[] = db_quote("JOIN ?:staff_descriptions ON ?:staff_descriptions.staff_id = ?:staff.staff_id AND ?:staff_descriptions.lang_code = ?s", $lang_code);

    $condition = (AREA == 'A') ? '' : " AND ?:staff.status IN ('A')";

    $staff_member = db_get_array("SELECT " . implode(", ", $fields) . " FROM ?:staff " . implode(" ", $joins) . " $condition");

    if (!empty($staff_member)) {
        foreach ($staff_member as $key => $value) {
            $staff_member[$key]['image_pair'] = fn_get_image_pairs($staff_member[$key]['staff_id'], 'staff', 'M', true, false);
            if (empty($staff_member[$key]['image_pair'])) {
                unset($staff_member[$key]);
            }
        }        
    }

    foreach ($staff_member as $key => &$value) {
        if ($value['user_id'] != 0) {
            $value['firsttname'] = (empty($value['firstname'])) ? db_get_field("SELECT firstname FROM ?:users WHERE user_id = ?i", $value['user_id']) : $value['firstname'];
            $value['lastname'] = (empty($value['lastname'])) ? db_get_field("SELECT lastname FROM ?:users WHERE user_id = ?i", $value['user_id']) : $value['lastname'];
            $value['email'] = (empty($value['email'])) ? db_get_field("SELECT email FROM ?:users WHERE user_id = ?i", $value['user_id']) : $value['email'];
        }     
    }

    return $staff_member;
}