{if $staff_members}
    {assign var="id" value=$staff_members.staff_id}
{else}
    {assign var="id" value=""}
{/if}

{assign var="allow_save" value=$staff_members|fn_allow_save_object:"staff_members"}

{** staff members section **}
{capture name="mainbox"}

<form action="{""|fn_url}" method="post" class="form-horizontal form-edit  {if !$allow_save} cm-hide-inputs{/if}" name="staff_members_form" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="staff_id" value="{$id}" />

{capture name="tabsbox"}
<div id="content_general">
    <div class="control-group">
        <label for="staff_member_firstname" class="control-label cm-required">{__("first_name")}</label>
        <div class="controls">
        <input type="text" name="staff_data[firstname]" id="staff_member_firstname" value="{$staff_members.firstname}" size="25" class="input-large" /></div>
    </div>

    <div class="control-group">
        <label for="staff_member_lastname" class="control-label">{__("last_name")}</label>
        <div class="controls">
        <input type="text" name="staff_data[lastname]" id="staff_member_lastname" value="{$staff_members.lastname}" size="25" class="input-large" /></div>
    </div>

    <div class="control-group">
        <label for="staff_member_email" class="control-label">{__("email")}</label>
        <div class="controls">
        <input type="text" name="staff_data[email]" id="staff_member_email" value="{$staff_members.email}" size="25" class="input-large" /></div>
    </div>

    <div class="control-group cm-no-hide-input" id="staff_text">
        <label class="control-label" for="staff_member_function">{__("staff_function")}:</label>
        <div class="controls">
            <textarea id="staff_member_function" name="staff_data[function]" cols="35" rows="8" class="cm-wysiwyg input-large">{$staff_members.function}</textarea>
        </div>
    </div>

    <div class="control-group" id="staff_image">
        <label class="control-label">{__("image")}</label>
        <div class="controls">
            {include file="common/attach_images.tpl" image_name="staff_member" image_object_type="staff" image_pair=$staff_members.main_pair image_object_id=$id no_detailed=true hide_titles=true}
        </div>
    </div>

    <div class="control-group">
        <label for="staff_member_user_id" class="control-label">{__("user_id")}</label>
        <div class="controls">
        <input type="text" name="staff_data[user_id]" id="staff_member_user_id" value="{$staff_members.user_id}" size="25" class="input-large" /></div>
    </div>
    {include file="common/select_status.tpl" input_name="staff_data[status]" id="member_status" obj_id=$id obj=$staff_members.status hidden=true}
</div>
{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}

{capture name="buttons"}
    {if !$id}
        {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="staff_members_form" but_name="dispatch[staff_members.update]"}
    {else}
        {include file="buttons/save_cancel.tpl" but_name="dispatch[staff_members.update]" but_role="submit-link" but_target_form="staff_members_form" hide_first_button=$hide_first_button hide_second_button=$hide_second_button save=$id}
    {/if}
{/capture}
    
</form>

{/capture}

{if !$id}
    {assign var="title" value=__("staff_members.new_staff")}
{else}
    {assign var="title" value="{__("staff_members.editing_staff")}: `$staff_members.firstname` `$staff_members.lastname`"}
{/if}
{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons select_languages=true}

{** staff members **}
