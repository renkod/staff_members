{** staff members section **}

{capture name="mainbox"}

{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}

<form action="{""|fn_url}" method="post" name="staff_members_form" id="staff_members_form" class="cm-hide-inputs">

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}
{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}

{if $staff_members}
<table width="100%" class="table table-middle">
<thead>
<tr>
    <th width="1%" class="left">{include file="common/check_items.tpl" class="cm-no-hide-input"}</th>
    <th class="nowrap"><a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("person_name")}{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th class="nowrap"><a class="cm-ajax" href="{"`$c_url`&sort_by=function&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("staff_function")}{if $search.sort_by == "function"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th class="nowrap"><a class="cm-ajax" href="{"`$c_url`&sort_by=email&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("email")}{if $search.sort_by == "email"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th class="nowrap">{__("user")}</th>
    <th width="6%">&nbsp;</th>
    <th width="10%" class="right"><a class="cm-ajax" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
</tr>
</thead>

{foreach from=$staff_members item=member}

{assign var="allow_save" value=$member|fn_allow_save_object:"staff_members"}

{if $allow_save}
    {assign var="no_hide_input" value="cm-no-hide-input"}
{else}
    {assign var="no_hide_input" value=""}
{/if}

<tr class="cm-row-status-{$member.status|lower}">
    
    <td class="left"><input type="checkbox" name="member_ids[]" value="{$member.staff_id}" class="cm-item {$no_hide_input}" /></td>
    <td class="row-status"><a class="row-status" href="{"staff_members.update?staff_id=`$member.staff_id`"|fn_url}">{$member.firstname} {$member.lastname}</a></td>
    <td class="nowrap row-status {$no_hide_input}">{$member.function}</td>
    <td class="nowrap row-status {$no_hide_input}"><a class="row-status" href="mailto:{$member.email|escape:url}">{$member.email}</a></td>
    <td class="nowrap row-status {$no_hide_input}">{if $member.user_id != 0}<a class="row-status" href="{"profiles.update&user_id=`$member.user_id`"|fn_url}">{__("edit")}</a>{/if}</td>
    <td>
        {capture name="tools_list"}
            <li>{btn type="list" text=__("edit") href="staff_members.update?staff_id=`$member.staff_id`"}</li>
        {if $allow_save}
            <li>{btn type="list" class="cm-confirm cm-post" text=__("delete") href="staff_members.delete?staff_id=`$member.staff_id`"}</li>
        {/if}
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
    <td class="right">{include file="common/select_popup.tpl" id=$member.staff_id status=$member.status hidden=true object_id_name="staff_id" table="staff" popup_additional_class="`$no_hide_input` dropleft"}</td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id=$smarty.request.content_id}

{capture name="buttons"}
    {capture name="tools_list"}
        {if $staff_members}
            <li>{btn type="delete_selected" dispatch="dispatch[staff_members.m_delete]" form="staff_members_form"}</li>
        {/if}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}
{capture name="adv_buttons"}
    {include file="common/tools.tpl" tool_href="staff_members.add" prefix="top" hide_tools="true" title=__("add_member") icon="icon-plus"}
{/capture}

</form>

{/capture}
{include file="common/mainbox.tpl" title=__("staff_members") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons select_languages=true}

{** staff members **}