{$obj_prefix = "`$block.block_id`000"}

{if $block.properties.outside_navigation == "Y"}
    <div class="owl-theme ty-owl-controls">
        <div class="owl-controls clickable owl-controls-outside" id="owl_outside_nav_{$block.block_id}">
            <div class="owl-buttons">
                <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="ty-icon-left-open-thin"></i></div>
                <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="ty-icon-right-open-thin"></i></div>
            </div>
        </div>
    </div>
{/if}

<div id="scroll_list_{$block.block_id}" class="owl-carousel">
    {foreach from=$members item="member" name="for_members"}
        <div class="ty-center">
            {include file="common/image.tpl" assign="object_img" class="" image_width=$block.properties.thumbnail_width image_height=$block.properties.thumbnail_width images=$member.image_pair no_ids=true lazy_load=true obj_id="scr_`$block.block_id`000`$member.variant_id`"}
            {$object_img nofilter}<p><b>{$member.firstname} {$member.lastname}</b></p>{$member.function}
            <input type="hidden" class="cm-no-hide-input" id="{$member.staff_id}" value="{$member.staff_id}" />
            <div>
            {if $member.email}
                <a id="mail{$member.staff_id}" class="row-status"  onclick="fn_sd_change_mail('{$member.staff_id}');">{__("show_email")}</a>
            {/if}
            </div>      
        </div>
    {/foreach}
</div>

{include file="common/scroller_init.tpl" items=$members prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}

<script type ="text/javascript">
    function fn_sd_change_mail(staff_id)
    {
        $.ceAjax('request', fn_url('staff_members.update_email&staff_id='+staff_id), {
            method: 'POST',
            callback: function(data) {
                $('#mail'+staff_id).html('<a href="mailto:'+data.text+'">'+data.text+'</a>');
            }
        });
    }
</script>

